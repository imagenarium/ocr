from fastapi import FastAPI, UploadFile
from pydantic import BaseModel
import requests
import os
import uvicorn


api_key = os.environ['API_KEY']


app = FastAPI()


@app.post("/ocr", name = "Распознать текст на изображении", tags = ["OCR"])
def ocr(file: UploadFile):
    response = requests.post("https://api.ocr.space/Parse/Image",
                        files={'file': file.file.read()},
                        data={'filetype':'JPG', 'detectOrientation': 'true', 'scale': 'true', 'OCREngine': '2'},
                        headers={'apikey': api_key}
                        ).json()
    return {"result": response["ParsedResults"][0]["ParsedText"]}


if __name__ == "__main__":
    config = uvicorn.Config("main:app",
                            port=80,
                            host="0.0.0.0",
                            log_level="info",
                            workers=os.cpu_count())

    server = uvicorn.Server(config)
    server.run()
