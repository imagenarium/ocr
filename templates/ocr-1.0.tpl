<@requirement.NODE 'ocr' 'ocr-${namespace}' 'ocr-standby-${namespace}' />

<@requirement.PARAM name='API_PORT' value='8040' type='port' />
<@requirement.PARAM name='API_KEY' />

<@img.TASK 'ocr-${namespace}' 'imagenarium/ocr:1.0'>
  <@img.NODE_REF 'ocr' />
  <@img.PORT PARAMS.API_PORT '80' />
  <@img.ENV 'API_KEY' PARAMS.API_KEY />
  <@img.CHECK_PORT '80' />
</@img.TASK>
